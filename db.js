const mongoose = require("mongoose");
const mongodbconnect = async () => {
  try {
    const url = "mongodb://localhost:27017/resto-project";
    mongoose.set("useFindAndModify", false);
    return await mongoose.connect(url, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
    });
  } catch (err) {
    console.error(err);
    throw err;
  }
};
module.exports = mongodbconnect;
