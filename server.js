const mongodbconnect = require("./db");
const express = require("express");
//const course_router= require('./routers/courses')
const client_router = require("./routers/clients");
//const user_router= require('./routers/users')
const port = process.env.PORT || 3001;
const app = express();
app.use(express.json());
////app.use('/api/courses',course_router);
app.use("/api/clients", client_router);
//app.use('/api/users',user_router);
const mongodb = async () => {
  try {
    const connection = await mongodbconnect();

    if (connection) {
      console.log("db connect  ..");
      app.listen(port, () => console.log("Server on", port));
    }
  } catch (err) {
    console.error(err);
    throw err;
  }
};
mongodb();
