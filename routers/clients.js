const router = require("express").Router();
const _ = require("lodash");
const Client = require("../models/client");

router.get("", async (req, res) => {
  res.send(await Client.find());
});

router.post("", async (req, res) => {
  let client = new Client(_.pick(req.body, "name", "email", "address"));
  try {
    client = await client.save();
  } catch (error) {
    res.status(400).send("Save in DB Error" + error.message);
  }
  //console.log("hello there");
  res.send(client);
});

module.exports = router;
